#!/usr/bin/env python3
# -*- Coding:utf-8 -*-
"""
################################################################################
Implementation of View,Write,Reply,Forward windows:one class per kind.
Code is factored here for reuse :a Write window is a customized View window,
and Reply and Forward are custom Write windows.
Windows defined in this file are created by the list windows,
in response to user actions.
################################################################################
"""
from SharedNames import * #program-wide global objects

###############################################################################
# message view window - also a superclass of write,reply,forward
###############################################################################

from minghu6.internet import email as mailtools

class ViewWindow(windows.PopupWindow,mailtools.MailParser):
    """
    a Toplevel,with extra protocol and embedded TextEditor;
    inherits saveParts,partList from mailtools.MailParser;
    mixed in custom subclass logic by direct inheritance here;
    """

    modelabel='View' #used in window titles


    okayToOpenParts=getattr(mailconfig, 'okayToOpenParts', True)
    verifyPartOpens=getattr(mailconfig, 'verifyPartOpens', False)
    maxPartButtons=getattr(mailconfig, 'maxPartButtons', 8)
    skipTextOnHtmlPart=getattr(mailconfig, 'skipTextOnHtmlPart', False)

    tempPartDir='TempParts'

    #all view windows use same dialog:remembers last dir
    partsDialog=Directory(title=appname+': Select parts save directory')

    def __init__(self,headermap,showtext,origmessage=None):
        '''
        header map is origmessage,or custom hdr dict for writing;
        showtext is main part of the message:parsed or custom;
        origmessage is parsed email.message.Message for view mail windows
        :param headermap:
        :param showtext:
        :param origmessage:
        :return:
        '''
        windows.PopupWindow.__init__(self,appname,self.modelabel)
        self.origMessage=origmessage
        self.makeWidgets(headermap,showtext)

    def makeWidgets(self,headermap,showtext):
        '''
        add headers,actions ,attachments,text editor
        :param headermap:
        :param showtext:
        :return:
        '''
        actionsframe=self.makeHeaders(headermap)
        if self.origMessage and self.okayToOpenParts:
            self.makePartButtons()

        self.editor=textEditor.TextEditorComponentMinimal(self)
        myactions=self.actionButtons()
        for(label,callback) in myactions:
            b=Button(actionsframe,text=label,command=callback)
            b.config(bg='beige',relief=RIDGE,bd=2)
            b.pack(side=TOP,expand=YES,fill=BOTH)

        self.editor.pack(side=BOTTOM)
        self.update()
        self.editor.setAllText(showtext)
        lines=len(showtext.splitlines())
        lines=min(lines+3,getattr(mailconfig, 'viewheight', 20))
        self.editor.setHeight(lines)
        self.editor.setWidth(80)
        if getattr(mailconfig, 'viewbg', None):
            self.editor.setBg(mailconfig.viewbg)
        if getattr(mailconfig, 'viewfg', None):
            self.editor.setFg(mailconfig.viewfg)
        if getattr(mailconfig, 'viewfont', None):
            self.editor.setFont(mailconfig.viewfont)

    def makeHeaders(self,headermap):
        '''
        add header entry fields,return action buttons frames;
        :param headermap:
        :return:
        '''
        top=Frame(self)
        top.pack(side=TOP,fill=X)

        left=Frame(top)
        left.pack(side=LEFT,expand=NO,fill=BOTH)

        middle=Frame(top)
        middle.pack(side=LEFT,expand=YES,fill=X)

        #headers set may be extended in mailconfig (Bcc,other?)
        self.userHdrs=()
        showhdrs=('From','To','Cc','Subject')
        if getattr(mailconfig, 'viewheaders', None):
            self.userHdrs=mailconfig.viewheaders
            showhdrs+=self.userHdrs

        addhdrs=('From','To','Cc','Bcc')

        self.hdrFields=[]
        for(i,header) in enumerate(showhdrs):
            lab =Label(middle,text=header+':',justify=LEFT)
            ent=Entry(middle)
            lab.grid(row=i,column=0,sticky=EW)
            ent.grid(row=i,column=1,sticky=EW)
            middle.rowconfigure(i,weight=1)
            hdrvalue=headermap.get(header,'?')

            if header not in addhdrs:
                hdrvalue=self.decodeHeader(hdrvalue)
            else:
                hdrvalue=self.decodeAddrHeader(hdrvalue)

            try:
                ent.insert('0',hdrvalue)
            except:
                import traceback
                traceback.print_exc()
                print('En..., May be a bug of tcl/tk')

            self.hdrFields.append(ent)
        middle.columnconfigure(1,weight=1)
        return left

    def actionButtons(self):
        return [('Cancel',self.destroy),
                ('Parts',self.onParts),
                ('Split',self.onSplit)]

    def makePartButtons(self):

        def makeButton(parent,text,callback):
            link=Button(parent,text=text,command=callback,relief=SUNKEN)
            if getattr(mailconfig, 'partfg', None):
                link.config(fg=mailconfig.partfg)

            if getattr(mailconfig, 'partbg', None):
                link.config(bg=mailconfig.partbg)

            link.pack(side=LEFT,fill=X,expand=YES)

        parts=Frame(self)
        parts.pack(side=TOP,expand=NO,fill=X)
        for (count,partname) in enumerate(self.partsList(self.origMessage)):
            if count==self.maxPartButtons:
                makeButton(parts,'...',self.onSplit)
                break
            openpart=(lambda partname=partname:self.onOnePart(partname))
            makeButton(parts,partname,openpart)

    def onOnePart(self,partname):
        '''
        locate selected part for button and save and open;
        okay if multiple mails open:resaves each time selected;
        we could probably just use web browser directly here;

        Caveat:tempPartDir is relative to cwd - poss anywhere;
        Caveat:tempOartDir is never cleaned up:meight be large,
        could use tempfile module (just like the HTML main text
        part display code in onView of the list window client)
        :param partname:
        :return:
        '''
        try:
            savedir=self.tempPartDir
            message=self.origMessage
            (contype,savepath)=self.saveOnePart(savedir,partname,message)

        except:
            showerror(appname, 'Error while writing part file')
            printStack(sys.exc_info())
        else:
            self.openParts([(contype,os.path.abspath(savepath))])


    def onParts(self):
        '''
        show message part/attachment in pop-up window;
        uses same file naming scheme as save on Split;
        if non-multipart ,string part=full body text
        :return:
        '''

        partnames=self.partsList(self.origMessage)
        msg='\n'.join(['Message parts:\n']+partnames)
        showinfo(appname,msg)

    def onSplit(self):
        '''
        pop up save dir dialog and save all parts/attachments there;
        if desired,pop up HTML and multimedia parts in web browser,
        text in TextEditor,and well-known doctypes on windows;
        could show parts in View windows where embedded etxt editor
        would provide a save button ,but most are not readable text
        :return:
        '''
        savedir=self.partsDialog.show()
        if savedir:
            try:
                partfiles=self.saveParts(savedir,self.origMessage)
            except:
                showerror(appname, 'Error while writing part files')
                printStack(sys.exc_info())
            else:
                if self.okayToOpenParts:
                    self.openParts(partfiles)

    def askOpen(self,appname,prompt):
        if not self.verifyPartOpens:
            return True
        else:
            return askyesno(appname,prompt)

    def openParts(self,partfiles):
        '''
        auto-open well known and safe file types, but only if verified
        by the user in a pop up; other types must be opened manually
        from save dir;  at this point, the named parts have been already
        MIME-decoded and saved as raw bytes in binary-mode files, but text
        parts may be in any Unicode encoding;  PyEdit needs to know the
        encoding to decode, webbrowsers may have to guess or be told;

        caveat: punts for type application/octet-stream even if it has
        safe filename extension such as .html; caveat: image/audio/video
        could be opened with the book's playfile.py; could also do that
        if text viewer fails: would start notepad on Windows via startfile;
        webbrowser may handle most cases here too, but specific is better;
        :param partfiles:
        :return:
        '''

        def textPartEncoding(fullfilename):

            partname=os.path.basename(fullfilename)
            for(filename,contype,part) in self.walkNamedParts(self.origMessage):
                if filename==partname:
                    return part.get_content_charset()

            assert False,'Text part not found'

        for (contype,fullfilename) in partfiles:
            maintype=contype.split('/')[0]
            extension=os.path.splitext(fullfilename)[1]
            basename=os.path.basename(fullfilename)

            #HTML and XML text,web pages,some media
            if contype in ['text/html','text/xml']:
                browserOpened=False
                if self.askOpen(appname,'Open "%s" in browser?'%basename):
                    try:
                        webbrowser.open_new('file://'+fullfilename)
                        browserOpened=True
                    except:
                        showerror(appname,'Browser failed:trying editor')

                if not browserOpened or not self.skipTextOnHtmlPart:
                    try:
                        encoding=textPartEncoding(fullfilename)
                        textEditor.TextEditorMainPopup(parent=self,
                                                       winTitle=' - %s email part'
                                                                %(encoding or '?'),
                                                       localFirst=fullfilename,
                                                       loadEncode=encoding)

                    except:
                        showerror(appname, 'Error opening text viewer')


                #text/plain text/x-python,etc.;4E:encoding,may fail
                elif maintype=='text':
                    if self.askOpen(appname,'Open text part "%s"'%basename):
                        try:
                            encoding=textPartEncoding(fullfilename)
                            textEditor.TextEditorMainPopup(parent=self,
                                                           winTitle=' - %s email part'%
                                                                    (encoding or '?'),
                                                           loadFirst=fullfilename,
                                                           loadEncode=encoding)
                        except:
                            showerror(appname, 'Error opening text viewer')

                #multimedia types:Windows opens mediaplayer,imageviewer,etc.
                elif maintype in ['image','audio','video']:
                    if self.askOpen(appname,'Open media part "%s"?'%basename):
                        try:
                            webbrowser.open_new('file://' + fullfilename)
                        except:
                            showerror(appname, 'Error opening browser')

                #common Windows doccuments:Word,Excel,Adobe,archives,etc.
                elif (sys.platform[:3]=='win' and
                      maintype=='application'and
                      extension in ['.doc','.docx','.xls','.xlsx',
                                    '.pdf','.zip','.tar','.wmv']):

                    if self.askOpen(appname,'Open part "%s"?'%basename):
                        os.startfile(fullfilename)

                else:
                    msg='Cannot open part: "%s"\n Open manually in:"%s"'
                    msg=msg%(basename,os.path.dirname(fullfilename))
                    showinfo(appname,msg)



###############################################################################
# message edit windows - write, reply, forward
###############################################################################


class WriteWindow(ViewWindow, mailtools.MailSenderAuth):
    """
    customized view display for composed new mail
    inherits sendMessage from mailtools.MailSender
    """
    modelabel = 'Write'

    def __init__(self,headermap,starttext,
                 smtpserver=None, smtpuser=None, smtpPassword=None):

            ViewWindow.__init__(self,headermap,starttext)
            mailtools.MailSenderAuth.__init__(self,
                                              smtpserver=smtpserver,
                                              smtpuser=smtpuser,
                                              smtpPassword=smtpPassword)

            self.smtpPassword=smtpPassword

            self.attaches=[]
            self.openDialog=None

    def actionButtons(self):
        return [('Cancel',self.quit),
                ('Parts',self.onParts),
                ('Attach',self.onAttach),
                ('Send',self.onSend)]

    def onParts(self):
        # caveat: deletes not currently supported
        if not self.attaches:
            showinfo(appname, 'Nothing attached')
        else:
            msg = '\n'.join(['Already attached:\n'] + self.attaches)
            showinfo(appname, msg)

    def onAttach(self):
            '''
            attach a flle to the mail:
            name added here will be added as part on Send
            :return:
            '''
            if not self.openDialog:
                self.openDialog=Open(title=appname+': Select Attachment File')
            filename=self.openDialog.show()
            if filename:
                self.attaches.append(filename)

    def resolveUnicodeEncodings(self):

        def isTextKind(filename):
            contype,encoding=mimetypes.guess_type(filename)
            if contype is None or encoding is not None:
                return False

            maintype,subtype=contype.split('/',1)
            return maintype=='text'

        bodytextEncoding=getattr(mailconfig, 'mainTextEncoding', 'utf-8')
        if bodytextEncoding == None:
            asknow=askstring('PyMailGUI','Enter main text Unicode encoding name')
            bodytextEncoding=asknow or 'latin-1'

        if bodytextEncoding!='utf-8':
            try:
                bodytext=self.editor.getAllText()
                bodytext.encode(bodytextEncoding)
            except (UnicodeError,LookupError): #lookup:bad encoding name
                bodytextEncoding='utf-8'

        #resolve any text part attachment encodings
        attachesEncodings=[]
        config=getattr(mailconfig, 'attachmentTextEncoding', 'utf-8')
        for filename in self.attaches:
            if not isTextKind(filename):
                attachesEncodings.append(None)
            elif config!=None:
                attachesEncodings.append(config)
            else:
                prompt='Enter Unicode encoding name for %'%filename
                asknow=askstring('PyMailGUI',prompt)
                attachesEncodings.append(asknow or 'latin-1')

            #last chance:use utf-8
            choice=attachesEncodings[-1]
            if choice !=None and choice !='utf-8':
                try:
                    attachbytes=open(filename,'rb').read()
                    attachbytes.decode(choice)
                except (UnicodeError,LookupError,IOError):
                    attachesEncodings[-1]='utf-8'

        return bodytextEncoding,attachesEncodings

    def onSend(self):

        #resolve Unicode encoding for text parts;
        bodytextEncoding,attachesEncodings=self.resolveUnicodeEncodings()

        #get components from GUI ;
        fieldvalues=[entry.get() for entry in self.hdrFields]
        From,To,Cc,Subj=fieldvalues[:4]
        extraHdrs=[('Cc',Cc),('X-Mailer',appname+' (Python)')]
        extraHdrs+=list(zip(self.userHdrs,fieldvalues[4:]))
        bodytext=self.editor.getAllText()

        Tos=self.splitAddresses(To)
        for (ix,(name,value)) in enumerate(extraHdrs):
            if value:
                if value=='?':
                    extraHdrs[ix]=(name,'')
                elif name.lower() in ['cc','bcc']:
                    extraHdrs[ix]=(name,self.splitAddresses(value))

        #withdraw to disallow send during send
        # caveat: might not be foolproof - user may deiconify if icon visible
        self.withdraw()
        self.getPassword()
        popup=popuputil.BusyBoxNowait(appname,'Sending message')
        sendingBusy.inc()
        threadtools.startThread(
            action=self.sendMessage,
            args=(From,Tos,Subj,extraHdrs,bodytext,self.attaches,
                  saveMailSeparator,bodytextEncoding,attachesEncodings),
            context=(popup,),
            onExit=self.onSendExit,
            onFail=self.onSendFail
        )

    def onSendExit(self,popup):
        """
        erase wait window, erase view window, decr send count;
        sendMessage call auto saves sent message in local file;
        can't use window.addSavedMails: mail text unavailable;
        """

        popup.quit()
        self.destroy()
        sendingBusy.dec()

        sentname=os.path.abspath(getattr(mailconfig, 'sentmailfile', ''))
        if sentname in openSaveFiles.keys():
            window=openSaveFiles[sentname]
            window.loadMailFileThread()

    def onSendFail(self,exc_info,popup):
        popup.quit()
        self.deiconify()
        self.lift()
        showerror(appname,'Send failed: \n%s\n%s'%exc_info[:2])
        printStack(exc_info)
        mailtools.MailSenderAuth.smtpPassword=None
        sendingBusy.dec()

    def askSmtpPassword(self,password=None):
        '''
        get password if needed from GUI here, in main thread;

        Caveat: may try this again in thread if no input first
        time, so goes into a loop until input is provided;
        :return:
        '''

        while not password:
            prompt=('Password for %s on %s?'%
                    (self.smtpUser,self.smtpServerName))
            password=popuputil.askPasswordWindow(appname,prompt,
                                                 default=self.smtpPassword)

        return password

class ReplyWindow(WriteWindow):
    """
    customized write display for replaying
    text and headers set up by list window
    """
    modelabel = 'Reply'

class ForwardWindow(WriteWindow):
    """
    customized reply display for forwarding
    text and headers set up by list window
    """
    modelabel = 'Forward'
























































