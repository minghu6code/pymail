#!/usr/bin/env python3
# -*- Coding:utf-8 -*-
"""
################################################################################
Help Doc
################################################################################
"""

helptext="""PyMail -- a Python/tkinter email client
软件运行环境：



	因为完全是标准python实现（更准确是说是python3）
	因此可以在任何Linux发行版直接运行,
	或者在安装了python3的Windows/Mac OS X上运行
	（核心主程序入口是PyMailGui.py，
	  通过PyMailGui_launch.py来检测mailconfig，实现多账户启动）




软件特性描述：



	没什么好说的，邮件客户端就是用来收邮件和发邮件的。
	虽然简陋，但仍然支持：包括但不限于
	                 1. 发送任意数量附件(press parts button to show)
                     2. 多账户
                     3. 邮件的本地保存与阅览
                     4. 编码方案、背景主题等基于配置文件的定制



操作指南：

	跟着感觉就对了，（Fwd用一个简陋的html2text解析html，View在默认浏览器中打开文本）



配置说明：



	本软件的个人定制在相当大程度上，依赖配置文件：
    而对于使用来讲，显然只有账户配置accounts是必须的，其余保持默认即可。
    下面是与系统运行关键配置的说明

    accounts 参数说明：

        利用POP3/IMAP 协议管理邮件服务器上的邮件
        popusername 账户名

        poppasswd   密码
        popservername 邮件服务器地址



        利用SMTP 协议发送邮件
        smtpuser
        smtppasswd
        smtpservername

        一个例子:

        accounts={'a7674321@163.com':{'popserver' : 'pop.163.com',
                                      'popuser'   : 'a7674321@163.com',
                                      'poppasswd' : 'a123456',
                                      'smtpserver': 'smtp.163.com',
                                      'smtpuser'  : 'a7674321',
                                      'smtppasswd': 'a123456'},

                  'a66741@sina.com':{'popserver'  : 'imap.sina.com',
                                     'popuser'    : 'a66741@sina.com',
                                     'poppasswd'  : 'a88587578',
                                     'smtpserver' : 'smtp.sina.com',
                                     'smtpuser'   : 'a66741',
                                     'smtppasswd' : 'a88587578'}}


	选择编码方式
	mainTextEncoding
	attachmentTextEncoding

	fetchlimit 可以设置最大抓取的邮件header的数量，（按照收件时间排序）
	           超出数量的邮件将显示 --mail skipperd--

	设置连接到邮件服务器的超时时间
	timeout = 20



"""

def showHtmlHelp():
    raise Exception('Not Implemeted')

if __name__=='__main__':
    print(helptext)
    input('Press Ente Key')