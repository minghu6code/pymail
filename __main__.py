# -*- Coding:utf-8 -*-
#!/usr/bin/env python3

"""

"""

import mailconfig
import sys
from SharedNames import appname,windows
from ListWindows import PyMailServer,PyMailFile


#use icon file in cwd or default in tools dir
srvrname=getattr(mailconfig, 'myaddress', 'Server')

class PyMailServerWindow(PyMailServer,windows.MainWindow):
    #a Tk,with extra protocol and mixed-in methods
    def __init__(self, *args, **kwargs):

        if {'popserver',
            'popuser',
            'pop_passwd',
            'srvrname',
            'smtpserver',
            'smtpuser',
            'smtpPassword'}.issubset(kwargs.keys()):

            popserver=kwargs['popserver']
            popuser=kwargs['popuser']
            pop_passwd=kwargs['pop_passwd']
            srvrname=kwargs['srvrname']

            smtpserver=kwargs['smtpserver']
            smtpuser=kwargs['smtpuser']
            smtpPassword=kwargs['smtpPassword']

            windows.MainWindow.__init__(self,appname,srvrname)
            PyMailServer.__init__(self,
                                  popserver=popserver,
                                  popuser=popuser,
                                  pop_passwd=pop_passwd,
                                  smtpserver=smtpserver,
                                  smtpuser=smtpuser,
                                  smtpPassword=smtpPassword)





        else:
            raise Exception(('lack essential map pairs'
                             '{popserver, popuser, pop_passwd, srvrname}'))



class PyMailServerPopup(PyMailServer,windows.PopupWindow):
    #a TopLevel,with extra protocol and mixed-in methods
    def __init__(self, srvrname, *args, **kwargs):
        windows.PopupWindow.__init__(self,appname,srvrname)
        PyMailServer.__init__(self, *args, **kwargs)

class PyMailServerComponent(PyMailServer,windows.ComponentWindow):
    #a Frame,with extra protocol and mixed-in methods
    def __init__(self, *args, **kwargs):
        windows.ComponentWindow.__init__(self)
        PyMailServer.__init__(self, *args, **kwargs)

class PyMailFileWindow(PyMailFile,windows.PopupWindow):
    #a Toplevel,with extra protocol and mixed-in methods
    def __init__(self,filename, *args, **kwargs):
        windows.PopupWindow.__init__(self,appname,filename)
        PyMailFile.__init__(self,filename, *args, **kwargs)


###############################################################################
# when run as a top-level program: create main mail-server list window
###############################################################################

def main_old():
    rootwin=PyMailServerWindow() #open server window
    if len(sys.argv)>1:
        for savename in sys.argv[1:]:
            rootwin.onOpenMailFile(savename)
        rootwin.lift() #save files loaded in threads
    rootwin.mainloop()



def main_interactive(popserver, popuser, pop_passwd, titlename,
                     smtpserver, smtpuser, smtpPassword):

    rootwin=PyMailServerWindow(popserver=popserver,
                               popuser=popuser,
                               pop_passwd=pop_passwd,
                               srvrname=titlename,
                               smtpserver=smtpserver,
                               smtpuser=smtpuser,
                               smtpPassword=smtpPassword)

    rootwin.mainloop()
    #return rootwin


def interactive():

    from argparse import ArgumentParser

    parser=ArgumentParser()

    parser.add_argument('-s', '--popserver',
                        help=('popserver name\n'
                              '(essential)'))

    parser.add_argument('-u', '--popuser',
                        help='popuser name (essential)')

    parser.add_argument('-p', '--pop_passwd',
                        help='password (essential)')

    parser.add_argument('-t', '--title', dest='titlename',
                        help='title of the GUI (optional)')

    parser.add_argument('--smtpserver',
                        help='smtp server name (essential)')

    parser.add_argument('--smtpuser',
                        help='smtp user name (essential)')

    parser.add_argument('--smtpPassword',
                        help='smtp password (essential)')


    from minghu6.algs.dict import remove_value
    args=remove_value(parser.parse_args().__dict__,None)

    if {'popserver', 'popuser', 'pop_passwd',
        'smtpserver', 'smtpuser'}.issubset(args):

        if 'titlename' not in args:
            args['titlename']=args['popuser']

        if 'smtpPassword' not in args:
            args['smtpPassword']=args['pop_passwd']


        return args

    else:
        raise Exception('lack essential args')


if __name__ == '__main__':

    args=interactive()
    root=main_interactive(**args)
    #root.mainloop()
