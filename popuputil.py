#!/usr/bin/env python3
# -*- Coding:utf-8 -*-

"""
#############################################################################
utility windows - may be useful in other programs
#############################################################################
"""

from tkinter import *
from minghu6.gui.windows import PopupWindow

class HelpPopup(PopupWindow):

    myfont='system' #customizable
    mywidth=78

    def __init__(self,appname,helptext,iconfile=None,
                 showsource=lambda:0):

        PopupWindow.__init__(self,appname,'Help',iconfile)
        from tkinter.scrolledtext import ScrolledText
        bar=Frame(self)
        bar.pack(side=BOTTOM,fill=X)
        code=Button(bar,bg='beige',text='Source',command=showsource)
        quit=Button(bar,bg='beige',text='Cancel',command=self.destroy)
        code.pack(pady=1,side=LEFT)
        quit.pack(pady=1,side=LEFT)

        text=ScrolledText(self)
        text.config(font=self.myfont)
        text.config(width=self.mywidth)
        text.config(bg='steelblue',fg='white')
        text.insert('0.0',helptext)
        text.pack(expand=YES,fill=BOTH)
        self.bind('<Return>',(lambda event:self.destroy()))



def askPasswordWindow(appname, prompt, default=''):
    """
    modal a dialog to input passwd string
    getpass.getpass used stdin,not GUI
    tkSimpleDialog.askstring echos input
    :param appname:
    :param prompt:
    :return:
    """
    win=PopupWindow(appname,'Prompt') #a configured Toplevel
    Label(win,text=prompt).pack(side=LEFT)
    entvar=StringVar(win)
    ent=Entry(win,textvariable=entvar,show='*') #display * for input
    ent.pack(side=RIGHT, expand=YES, fill=X)
    ent.bind('<Return>',lambda event:win.destroy())
    ent.focus_set()
    win.grab_set()
    win.wait_window()
    win.update()


    result=entvar.get().strip() or default
    #print('hi???\n', result)
    return result

class BusyBoxWait(PopupWindow):
    """
    POP UP blocking wait message box:
    thread waits main GUI event thread stays alive during wait
    but GUI is inoperable during this wait state;
    uses quit redef here because lower,not leftmost;
    """

    def __init__(self,appname,message):
        PopupWindow.__init__(self,appname,'Busy')
        self.protocol('WM_DELETE_WINDOW',lambda:0)
        label=Label(self,text=message+'...')
        label.config(height=10,width=40,cursor='watch')
        label.pack()
        self.makeModal()
        self.message,self.label=message,label

    def makeModal(self):
        self.focus_set()
        self.grab_set()

    def changeText(self,newtext):
        self.label.config(text=self.message+':'+newtext)

    def quit(self):
        self.destroy()

class BusyBoxNowait(BusyBoxWait):
    """
    POP UP noblocking wait window
    call changeText to show progress,quit to close
    """
    def makeModal(self):
        pass


if __name__=='__main__':

    #HelpPopup('spam','See figure 1...\n')
    print(askPasswordWindow('spam','enter password'))
    #input('Enter to exit')
































