# -*- Coding:utf-8 -*-
#!/usr/bin/env python3

"""
################################################################################
Launch for PyMailGUI
  solve multi-account issue
################################################################################
"""
#import mailconfig
import os

import mailconfig

main_file_name='PyMailGui.py'
def main():


    from minghu6.etc.launchmods import Popen as launcher

    accounts=mailconfig.accounts
    popens=[]

    for account in accounts.values():

        cmd=' '.join([str(main_file_name),
                      '--popserver    {0}'.format(account['popserver']),
                      '--popuser      {0}'.format(account['popuser']),
                      '--pop_passwd   {0}'.format(account['poppasswd']),
                      '--title        {0}'.format(account['popuser']),
                      '--smtpserver   {0}'.format(account['smtpserver']),
                      '--smtpuser     {0}'.format(account['smtpuser']),
                      '--smtpPassword {0}'.format(account['smtppasswd'])])

        #print(cmd)
        launch=launcher(label=account['popuser'],command=cmd)
        launch()
        popens.append(launch.popen)

    #main Process Can not return before SubProcess Return!!
    for popen in popens:
        popen.wait()




def interactive_shell():
    from argparse import ArgumentParser

    parser=ArgumentParser()

if __name__ == '__main__':

    main()