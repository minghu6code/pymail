#!/usr/bin/env python3
# -*- Coding:utf-8 -*-
"""
################################################################################
objects shared by all window classes and main file:
program-wide globals
################################################################################
"""
#(fix python3.3+ formataddr chage)
import py33patch

#used in all window,icon titles
appname='PyMailGUI'

#used for list save,open,delete;also for sent message file
saveMailSeparator=appname+('-'*60)+appname+'\n'

#currently viewd mail save files;also for sent-mail file
openSaveFiles={} #1 window per file,{name:win}

#standard library services
import sys
import os
import email.utils
import email.message
import webbrowser
import mimetypes



from tkinter import *
from tkinter.simpledialog import  askstring
from tkinter.filedialog import SaveAs,Open,Directory
from tkinter.messagebox import  showinfo,showerror,askyesno

#reuse minghu6 library


from minghu6.gui import windows,threadtools,textEditor
from minghu6.etc import wraplines

#modules defined here
#import mailconfig

import mailconfig

import popuputil
import messagecache
import html2text
import PyMailGuiHelper

def printStack(exc_info):
    """
    debugging: show exception and stack traceback on stdout;
    :param exc_info:
    :return:
    """
    print(exc_info[0])
    print(exc_info[1])
    import traceback
    try:
        traceback.print_tb(exc_info[2],file=sys.stdout)
    except:
        log=open('_pymailerrlog.txt','a') #use a real file
        log.write('-'*80)
        traceback.print_tb(exc_info[2],file=log)




#thread busy counters for threads run by this GUI
#sendingBusy shared by all send windows,used by main window quit

loadingHdrsBusy=threadtools.ThreadCounter()
deletingBusy=threadtools.ThreadCounter()
loadingMsgsBusy=threadtools.ThreadCounter()
sendingBusy=threadtools.ThreadCounter()






























