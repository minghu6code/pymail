# -*- Coding:utf-8 -*-


accounts = {'a7674321@163.com': {'popserver': 'pop.163.com',
                                 'popuser': 'a7674321@163.com',
                                 'poppasswd': '123456',
                                 'smtpserver': 'smtp.163.com',
                                 'smtpuser': 'a7674321',
                                 'smtppasswd': '123456'},

            '173225474@qq.com': {'popserver': 'pop.qq.com',
                                 'popuser': '173225474@qq.com',
                                 'poppasswd': '123456',
                                 'smtpserver': 'smtp.qq.com',
                                 'smtpuser': 'a19678zy',
                                 'smtppasswd': '123456'}}


timeout = 20  # Timeout of connect to POP server

import os
mysourcedir = os.path.dirname(os.getcwd())
sentmailfile = os.path.join(mysourcedir, 'sentmail.txt')
savemailfile = ''
poppasswdfile = ''
smtppasswdfile = ''

myaddress = 'a19678zy@163.com'
mysignature = ('Thanks,\n'
               '--minghu6')


mainTextEncoding = 'utf-8'
attachmentTextEncoding = 'utf-8'

fetchEncoding = 'utf-8'

fetchlimit = 50

showHelpAsText = True
showHelpAsHTML = False

wrapsz = 90

repliesCopyToAll = True   # True=reply to sender + all recipients, else sender


listbg = 'indianred'                  # None, 'white', '#RRGGBB'
listfg = 'black'
listfont = ('courier', 9, 'bold')       # None, ('courier', 12, 'bold italic')
# use fixed-width font for list columns
viewbg = 'light blue'               # was '#dbbedc'
viewfg = 'black'
viewfont = ('courier', 10, 'bold')
viewheight = 20                         # max lines for height when opened (20)

partfg = None
partbg = None

listWidth = 81           # None = use default 74
listHeight = 15          # None = use default 15


headersEncodeTo = None

skipTextOnHtmlPart = False
okayToOpenParts = True
verifyPartOpens = False

maxPartButtons = 8
